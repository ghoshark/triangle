/* ------------------------------
 * File: Main.java
 * Name: Dan Fleck
 * Assignment: N/A
 * Lab Section: All of them 
 * Creation Date: Nov 10, 2008
 * 
 * References:
 * Comments:
 * ------------------------------
 */

package junitexample;

/**
 *
 * @author dfleck
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Triangle t = new Triangle("13", "13", "13");
        System.out.println("t is:"+t.determineTriangleType());
    }

}
